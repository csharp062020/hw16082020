﻿using System.ComponentModel.Design.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace hw16082020
{
    public class Car
    {

        public int Model { get; set; }
        public string Brand { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        private int _coden;

        protected int _numberOfSeats;

        public Car()
        {
            
        }

        public Car(string fileName)
        {
            
        }
        public Car(int model, string brand, int year, string color, int coden, int numberOfSeats)
        {
            _coden = coden;
            _numberOfSeats = numberOfSeats;
            Model = model;
            Brand = brand;
            Year = year;
            Color = color;
        }

        public static void SerializeACar(string fileName, Car car)
        {
            XmlSerializer carXmlSerializer = new XmlSerializer(typeof(Car));

            using (Stream xmlFileStream = new FileStream(fileName, FileMode.Create))
            {
                carXmlSerializer.Serialize(xmlFileStream, car);
            }

        }
        public static void SerializeACarArray(string fileName, Car[] cars)
        {
            XmlSerializer carXmlSerializer = new XmlSerializer(typeof(Car[]));

            using (Stream xmlFileStream = new FileStream(fileName, FileMode.Create))
            {
                carXmlSerializer.Serialize(xmlFileStream, cars);
            }

        }

        public static Car DeserializeACar(string fileName)
        {
            Car DeserializeCar = null;
            XmlSerializer carXmlSerializer = new XmlSerializer(typeof(Car));
            using (Stream xmlFileStream = new FileStream(fileName, FileMode.Open))
            {
                DeserializeCar = carXmlSerializer.Deserialize(xmlFileStream) as Car;
            }

            return DeserializeCar;
        }
        public static Car[] DeserializeCarArray(string fileName)
        {
            Car[] DeserializeCarArray = null;
            XmlSerializer carXmlSerializer = new XmlSerializer(typeof(Car[]));
            using (Stream xmlFileStream = new FileStream(fileName, FileMode.Open))
            {
                DeserializeCarArray = carXmlSerializer.Deserialize(xmlFileStream) as Car[];
            }

            return DeserializeCarArray;

        }

        public bool CarCompare(string fileName)
        {
            Car DeserializeCar = Car.DeserializeACar(fileName);
            return (this.Model == DeserializeCar.Model &&
                    this.Brand == DeserializeCar.Brand &&
                    this.Year == DeserializeCar.Year &&
                    this.Color == DeserializeCar.Color);
        }

        public int GetCoded()
        {
            return _coden;
        }

        public int GetNumberOfSeats()
        {
            return _numberOfSeats;
        }

        protected void ChangeNumberOfSeats(int numberOfSeats)
        {
            _numberOfSeats = numberOfSeats;
        }

        public override string ToString()
        {
            return $"{base.ToString()}, Modle : {this.Model} Brand : {this.Brand} Year : {this.Year} Color : {this.Color}";
        }
    }
}