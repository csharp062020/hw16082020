﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace hw16082020
{
    class Program
    {
        static void Main(string[] args)
        {
            Step1();

            Step2Etgar();
        }

        private static void Step2Etgar()
        {
            string carFilePath = Directory.GetCurrentDirectory() + @"\carSe.xml";
            Car car = new Car(1999, "MB", 1989, "Black", 555, 5);
            Car.SerializeACar(carFilePath, car);
            Console.WriteLine($"car send: {car.ToString()}");
            Car carDes = Car.DeserializeACar(carFilePath);
            Console.WriteLine($"car Get: {carDes.ToString()}");

            Console.WriteLine($"car is the same? {car.CarCompare(carFilePath)}");
            Console.WriteLine("change car Model");
            car.Model = 2020;
            Console.WriteLine(car.ToString());
            Console.WriteLine($"car on file: {carDes.ToString()}");
            Console.WriteLine($"car is the same? {car.CarCompare(carFilePath)}");


            string carsFilePath = Directory.GetCurrentDirectory() + @"\carArraySe.xml";
            Car[] cars =
            {
                new Car(1999, "MB", 1989, "Black", 555, 5),
                new Car(1999, "MB", 1990, "Black", 555, 5),
                new Car(1999, "MB", 1991, "Black", 555, 5),
                new Car(1999, "MB", 1992, "Black", 555, 5),
                new Car(1999, "MB", 1993, "Black", 555, 5)
            };

            Car.SerializeACarArray(carsFilePath, cars);
            Car[] carsDe = Car.DeserializeCarArray(carsFilePath);
        }

        private static void Step1()
        {
            Car car1 = new Car(1999, "MB", 1989, "Black", 555, 5);
            Car readCar;
            XmlSerializer carXmlSerializer = new XmlSerializer(typeof(Car));

            using (Stream xmlFileStream = new FileStream(@"..\..\Car.xml", FileMode.Create))
            {
                carXmlSerializer.Serialize(xmlFileStream, car1);
            }

            using (Stream xmlFileStream = new FileStream(@"..\..\Car.xml", FileMode.Open))
            {
                readCar = carXmlSerializer.Deserialize(xmlFileStream) as Car;
            }

            Car[] cars =
            {
                new Car(1999, "MB", 1989, "Black", 555, 5),
                new Car(1999, "MB", 1990, "Black", 555, 5),
                new Car(1999, "MB", 1991, "Black", 555, 5),
                new Car(1999, "MB", 1992, "Black", 555, 5),
                new Car(1999, "MB", 1993, "Black", 555, 5)
            };

            Car[] readCars;
            XmlSerializer carArryXmlSerializer = new XmlSerializer(typeof(Car[]));

            using (Stream xmlFileStream = new FileStream(@"..\..\Cars.xml", FileMode.Create))
            {
                carArryXmlSerializer.Serialize(xmlFileStream, cars);
            }

            using (Stream xmlFileStream = new FileStream(@"..\..\Cars.xml", FileMode.Open))
            {
                readCars = carArryXmlSerializer.Deserialize(xmlFileStream) as Car[];
            }
        }
    }
}
